package com.itls.providertest;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class MyDao {
    private final Context context;
    private final SQLiteDatabase db;

    public MyDao(Context context) {
        this.context = context;
        MyDBHelper myDBHelper = new MyDBHelper(context, "zDB", null, 1);
        db=myDBHelper.getWritableDatabase();
    }

    public Uri DaoInsert(ContentValues values) {
        long rowId=db.insert("user",null,values);
        Uri uri=Uri.parse("content://zz.provider/user");
        Uri newUri= ContentUris.withAppendedId(uri,rowId);
        context.getContentResolver().notifyChange(newUri,null);
        return newUri;
    }
}
