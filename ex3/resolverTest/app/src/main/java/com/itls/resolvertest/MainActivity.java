package com.itls.resolvertest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ContentResolver resolver=getContentResolver();
        ContentValues values=new ContentValues();
        values.put("name","zz");
        values.put("age",20);
        Button button=findViewById(R.id.button);
        Uri uri=Uri.parse("content://zz.provider/user");
        button.setOnClickListener(v->{
            resolver.insert(uri,values);
        });
    }
}