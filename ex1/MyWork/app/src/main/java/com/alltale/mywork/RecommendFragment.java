package com.alltale.mywork;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import java.util.ArrayList;
import java.util.List;

public class RecommendFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<Recommend> recommendList = new ArrayList<>();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    public RecommendFragment() {
        // Required empty public constructor
    }

    public static RecommendFragment newInstance(String param1, String param2) {
        RecommendFragment fragment = new RecommendFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_recommend, container, false);
        recyclerView = root.findViewById(R.id.fragment_recommend);
        //设置RecyclerView保持固定大小,这样可以优化RecyclerView的性能
        recyclerView.setHasFixedSize(true);
        initData();
        //设置瀑布流布局为2列，垂直方向滑动
        recyclerView.setLayoutManager(
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        MyAdapter staggeredGridAdapter = new MyAdapter(this.getActivity(), recommendList);
        recyclerView.setAdapter(staggeredGridAdapter);
        return root;
    }

    /**
     * 数据源
     */
    private void initData() {
        String[] names = new String[]{"粉色海洋", "草原落日", "山上日出", "雪中的树",
                "湖面倒影", "向日葵", "倒映天光", "黄沙漫漫", "枫","晚霞"};
        int[] ImageId = new int[]{R.drawable.img_1, R.drawable.img_8, R.drawable.img_5,
                R.drawable.img_4, R.drawable.img_7, R.drawable.img_6, R.drawable.img_3,
                R.drawable.img_2, R.drawable.img_9,R.drawable.img};
        for (int i = 0; i < names.length; i++) {
            this.recommendList.add(new Recommend(names[i], ImageId[i]));
        }
    }
}