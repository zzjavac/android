package com.alltale.mywork;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.RecommendViewHolder> {
 
    private Context mContext;
    private List<Recommend> recommendList;
 
    public MyAdapter(Context context, List<Recommend> recommendList) {
        this.mContext = context;
        this.recommendList = recommendList;
    }

    /**
     * 创建列表组件
     *
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public MyAdapter.RecommendViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_item, null);
        return new MyAdapter.RecommendViewHolder(view);
    }
 
    /**
     * 绑定数据
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull RecommendViewHolder holder, int position) {
        holder.nameTv.setText(recommendList.get(position).getName());
        holder.imageIdTv.setImageResource(recommendList.get(position).getImageId());
        //设置点击效果
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ItemActivity.class);
                mContext.startActivity(intent);
            }
        });
    }
 
    @Override
    public int getItemCount() {
        return recommendList.size();
    }
 
 
    public class RecommendViewHolder extends RecyclerView.ViewHolder {
        TextView nameTv;
        ImageView imageIdTv;
        LinearLayout linearLayout;          //点击

        public RecommendViewHolder(View view) {
            super(view);
            this.nameTv = view.findViewById(R.id.recommend_item_text);
            this.imageIdTv = view.findViewById(R.id.recommend_item_image);
            //点击效果
            linearLayout = itemView.findViewById(R.id.recycle_item);
        }
        //点击效果
    }
}